describe('Todo App', function () {
  it('should add a new todo item "Pay Bills"', function () {
    // 1. Add a test that visits http://localhost:3000
    // 1.1 The test should type "Pay Bills" and adds a new todo item.
    // 1.2 The test should assert the table contains the new "Pay Bills" item. 
  })

  it('should find and ticks the todo item that contains "Fill Gas"', function () {
    // 2. Add a test that visits http://localhost:3000 
    // 2.1. The test should complete (tick) the item that contains "Fill Gas" 
    // 2.2. The test should assert the td has a class of "done".
  })

  it('should find and delete a todo item that contains "Buy Milk"', function () {
    // 3. Add a test that visits http://localhost:3000
    // 3.1. The test should delete the item that contains "Buy Milk".
    // 3.2. The test should assert the table has a length of 2.
  })
});
