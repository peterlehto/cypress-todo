# Cypress Todo Test

This is a simple JavaScript End to End testing playground using Cypress.io. This test uses a React application as an example Todo app to write integration tests.

## Setup your GitLab profile

If you don't already have a GitLab profile setup you'll have to create one and apply your computers SSH key to the profile. This will allow you to pull and push to this repository.

Follow the steps to [setup your GitLab and SSH key here](https://subscription.packtpub.com/book/application_development/9781783986842/2/ch02lvl1sec20/adding-your-ssh-key-to-gitlab)

## Setup

1. `git clone git@gitlab.com:peterlehto/cypress-todo.git`
2. `cd cypress-todo`
3. `npm install`

### How to run the test suite

1. Open 2 terminal tabs
2. In the first terminal run `npm run start`
3. In the second terminal run `npm run cypress:open`
4. A chrome window will pop up
5. Run the `todo_spec.js` test

### What is already setup and provided in this test

- Cypress is setup, use the command `npm run cypress:open` to run your tests.
- `todo_spec.js` has been created in `./cypress/integration/todo_spec.js`
  - `todo_spec.js` file has 3 test cases that need to be completed.
- `TodoForm.js` component has the following `data-cy` attributes:
  - `data-cy="newItemField"`
- `TodoListItem.js` component has the following `data-cy` attributes:
  - `data-cy='todoItem'`
  - `data-cy="markAsCompleted"`
  - `data-cy="markAsDeleted"`

### The Test

Please create a new branch using your name and date as the branch name eg. `peter-15-06-2021`

Complete the following tests:

- **Add a todo item**
  - Add a test that visits http://localhost:3000
  - The test should type "Pay Bills" and adds a new todo item.
  - The test should assert the table contains the new "Pay Bills" item.
- **Complete a todo item**
  - Add a test that visits http://localhost:3000
  - The test should complete (tick) the item that contains "Fill Gas"
  - The test should assert the td has a class of "done".
- **Delete a todo item**
  - Add a test that visits http://localhost:3000
  - The test should delete the item that contains "Buy Milk".
  - The test should assert the table has a length of 2.
